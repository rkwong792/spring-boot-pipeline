package springbootpipeline.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        try {
            int[] myNumbers = {1, 2, 3};
            System.out.println(myNumbers[10]);
        } catch (Exception ex) {
            System.out.println("Something went wrong");
        }

        checkAge(20);

        //supplyAsync() - Run a task asynchronously and return the result.
        // Using Lambda Expression
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            return "Result of the asynchronous computation";
        });


        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            return "Result of the asynchronous computation";
        });

        String result = future.get();
        System.out.println(result);

        System.out.println(CompletableFuture.allOf(future,future2).join());

    }

        public static void checkAge (int age){
            if (age < 18) {
                throw new ArithmeticException("Access denied - You must be at least 18 years old");
            }
            else {
                System.out.println("GRANTED");
            }
        }

        public static void waitMethod (long time) throws InterruptedException {
            TimeUnit.SECONDS.sleep(time);
        }

}
