package springbootpipeline.demo.controller;

import org.springframework.core.io.Resource;
import springbootpipeline.demo.model.Person;
import springbootpipeline.demo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Hashtable;

@RestController
@RequestMapping("/v1")
public class PersonController {

    private final PersonService ps;

    @Autowired
    public PersonController(PersonService ps){
        this.ps = ps;
    }

    //Returns all of the users.
    @RequestMapping("/all")
    public Hashtable<String,Person> getAll(){
        return ps.getAll();
    }

    //Returns one specific user based on passed in ID.
    @RequestMapping("/param/{id}")
    public Person getPerson(@PathVariable("id") String id) {
        return ps.getPerson(id);
    }


}